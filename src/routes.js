function loadPage(pagename){
  pagename += '.vue';
  return System.import('./pages/' + pagename);
}


export const routes = [
  {path: '/', component: () => loadPage('home')},
  {path: '/add', component: () => loadPage('add')},
];
