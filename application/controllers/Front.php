<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CC_Front {

  public function __construct()
  {
    parent::__construct();
  }
  public function _remap()
  {
    $segment_1 = $this->uri->segment(1);
    $segment_2 = $this->uri->segment(2);
    switch ($segment_1) {
      case '':
        $this->index();
        break;
      case 'add':
        $this->index();
        break;
      case 'edit':
        $this->index();
        break;
    }
  }
  public function index()
  {
    $data['title']='home';
    $this->blade_render->views('pages.home',$data);
  }

}
