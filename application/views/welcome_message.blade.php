<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Welcome message</title>
</head>
<body>

	<div id="app">
		{{--<img class="logo" src="{{assets('cheetah.png','image')}}" alt="">--}}
		{{--<h1 class="text-logo">CI-TAH</h1>--}}
	</div>
	<script src="{{assets('app.js','js')}}"></script>
</body>
</html>
