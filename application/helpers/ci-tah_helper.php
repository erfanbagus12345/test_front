<?php
/**
 * Created by PhpStorm.
 * User: Hatori-Harazawa
 * Date: 22/11/2018
 * Time: 23:14
 */

if (! function_exists('env')) {

	function env($name, $default = null) {

		if ($value = getenv($name)) {
			return $value;
		}

		return $default;
	}
}

if(!function_exists('my_test')){
	/**
	 * fungsi debug data return
	 * @param array $data
	 */
	function my_test($data=array())
	{
		echo "<pre>";
		var_dump($data);
		exit();
	}
}

if(!function_exists('myjson')){
	/**
	 * fungsi test json
	 * @param array $data
	 */
	function myjson($data=array()){
		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
	}
}

if(! function_exists('assets')){
	/**
	 * @param string $src
	 * @param null $folder
	 * @return string
	 */
	function assets($src='', $folder=null) {
		if($folder){
			return base_url('public/assets/'.$folder.'/'.$src);
		}else{
			return base_url('public/assets/'.$src);
		}
	}
}

if(! function_exists('active_link')){
	/**
	 * @param string $src
	 * @param null $folder
	 * @return string
	 */
	function active_link($link) {
		print_r(str_replace('cms','',uri_string()));
		if($link == str_replace('cms','',uri_string())) {
			return true;
		}else{
			return false;
		}
	}
}

